package eu.droidit.ondd.pdfcreator.pdfbox;

import org.apache.jempbox.xmp.XMPMetadata;
import org.apache.jempbox.xmp.XMPSchemaBasic;
import org.apache.jempbox.xmp.XMPSchemaDublinCore;
import org.apache.jempbox.xmp.XMPSchemaPDF;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDAppearance;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextbox;

import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import static org.apache.pdfbox.pdmodel.font.PDType1Font.HELVETICA_BOLD;

/**
 * Created with IntelliJ IDEA.
 * User: geroen
 * Date: 19/04/13
 * Time: 16:00
 * To change this template use File | Settings | File Templates.
 */
public class PdfCreatorWithPdfBox {

    private static final String PATH = "c:/temp/";
    private static final String FILE = PATH + "pdfFromPdfBox.pdf";
    private static final String WORKING_FILE = "c:/temp/FirstPdf.pdf";

    public static void main(String[] args) throws IOException, COSVisitorException, TransformerException {

        createNecessaryFiles();
        PDDocument document = createNewDocument();
        addMetaDataTo(document);
        PDPage page = addPageTo(document);
        addFormTo(page, document);
//        addContentTo(page, document);
        close(document);


    }

    private static void addFormTo(PDPage page, PDDocument document) throws IOException {
        // creating default resources
        PDDocumentCatalog documentCatalog = document.getDocumentCatalog();
        PDAcroForm acroForm = new PDAcroForm(document);
        documentCatalog.setAcroForm(acroForm);

        PDResources pdResources = new PDResources();
        pdResources.setFonts(createFontMap());
        acroForm.setDefaultResources(pdResources);
        page.setResources(pdResources);

        COSDictionary dic = acroForm.getDictionary();
        PDTextbox textField = new PDTextbox(acroForm, dic);
        PDRectangle rect = new PDRectangle();
        rect.setLowerLeftX(200);
        rect.setLowerLeftY(550);
        rect.setUpperRightX(150);
        rect.setUpperRightY(850);
        textField.getWidget().setRectangle(rect);
        textField.getDictionary().setName(COSName.FT, "Tx");
        textField.setPartialName("Test");
        textField.setRequired(true);
        textField.setReadonly(false);
        textField.setFileSelect(true);
        textField.setValue("Test");
        new PDAppearance(acroForm, textField);

//        page.getAnnotations().add(textField);
        page.getAnnotations().add(textField.getWidget());
        acroForm.getFields().add(textField);
    }

    private static Map< String, PDFont> createFontMap()
    {
        Map< String, PDFont > fonts = new HashMap< String, PDFont >();

        fonts.put( PDType1Font.COURIER.getBaseFont(), PDType1Font.COURIER );
        fonts.put( PDType1Font.COURIER_BOLD.getBaseFont(), PDType1Font.COURIER_BOLD );
        fonts.put( PDType1Font.COURIER_OBLIQUE.getBaseFont(), PDType1Font.COURIER_OBLIQUE );
        fonts.put( PDType1Font.COURIER_BOLD_OBLIQUE.getBaseFont(), PDType1Font.COURIER_BOLD_OBLIQUE
        );
        return fonts;
    }

    private static void addMetaDataTo(PDDocument document) throws IOException, TransformerException, COSVisitorException {
        PDDocumentCatalog catalog = document.getDocumentCatalog();


        XMPMetadata xmpMetadata = new XMPMetadata();
        XMPSchemaPDF xmpSchemaPDF = xmpMetadata.addPDFSchema();
        xmpSchemaPDF.setProducer("Geroen Joris");
        xmpSchemaPDF.setPDFVersion("1.0");

        XMPSchemaBasic schemaBasic = xmpMetadata.addBasicSchema();
        schemaBasic.setCreateDate(new GregorianCalendar());
        schemaBasic.setTitle("Test for ONDD PDF generation");

        XMPSchemaDublinCore schemaDublinCore = xmpMetadata.addDublinCoreSchema();
        schemaDublinCore.setTitle("Test for ONDD PDF generation");
        schemaDublinCore.setDescription("Test for ONDD PDF generation");
        schemaDublinCore.addCreator("Geroen Joris");


        PDMetadata metadata = new PDMetadata(document);
        metadata.importXMPMetadata(xmpMetadata);
        catalog.setMetadata(metadata);

    }

    private static void createNecessaryFiles() throws IOException {
        new File(PATH).mkdirs();
        new File(FILE).delete();
        new File(FILE).createNewFile();
    }

    private static PDDocument createNewDocument() throws IOException {
        PDDocument pdDocument = new PDDocument();
        System.out.println("Created new document at: " + FILE);
        return pdDocument;
    }

    private static PDPage addPageTo(PDDocument document) {
        PDPage page = new PDPage();
        document.addPage(page);
        return page;
    }

    private static void addContentTo(PDPage page, PDDocument document) throws IOException {
        PDFont font = HELVETICA_BOLD;
        PDPageContentStream contentStream = new PDPageContentStream(document, page);
        contentStream.beginText();
        contentStream.setFont( font, 12 );
        contentStream.moveTextPositionByAmount( 100, 700 );
        contentStream.drawString( "Hello World" );
        contentStream.endText();
        contentStream.close();
    }

    private static void close(PDDocument document) throws IOException, COSVisitorException {
        document.save(FILE);
        document.close();
    }

}
