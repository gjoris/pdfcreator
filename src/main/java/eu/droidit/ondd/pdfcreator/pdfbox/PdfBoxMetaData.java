package eu.droidit.ondd.pdfcreator.pdfbox;

import org.w3c.dom.*;

/**
 * Created with IntelliJ IDEA.
 * User: geroen
 * Date: 19/04/13
 * Time: 16:23
 * To change this template use File | Settings | File Templates.
 */
public class PdfBoxMetaData implements Document {
    @Override
    public DocumentType getDoctype() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public DOMImplementation getImplementation() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Element getDocumentElement() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Element createElement(String tagName) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public DocumentFragment createDocumentFragment() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Text createTextNode(String data) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Comment createComment(String data) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public CDATASection createCDATASection(String data) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ProcessingInstruction createProcessingInstruction(String target, String data) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Attr createAttribute(String name) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public EntityReference createEntityReference(String name) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public NodeList getElementsByTagName(String tagname) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node importNode(Node importedNode, boolean deep) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Element createElementNS(String namespaceURI, String qualifiedName) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Attr createAttributeNS(String namespaceURI, String qualifiedName) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public NodeList getElementsByTagNameNS(String namespaceURI, String localName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Element getElementById(String elementId) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getInputEncoding() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getXmlEncoding() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean getXmlStandalone() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setXmlStandalone(boolean xmlStandalone) throws DOMException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getXmlVersion() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setXmlVersion(String xmlVersion) throws DOMException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean getStrictErrorChecking() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setStrictErrorChecking(boolean strictErrorChecking) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getDocumentURI() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setDocumentURI(String documentURI) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node adoptNode(Node source) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public DOMConfiguration getDomConfig() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void normalizeDocument() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node renameNode(Node n, String namespaceURI, String qualifiedName) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getNodeName() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getNodeValue() throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setNodeValue(String nodeValue) throws DOMException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public short getNodeType() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node getParentNode() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public NodeList getChildNodes() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node getFirstChild() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node getLastChild() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node getPreviousSibling() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node getNextSibling() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public NamedNodeMap getAttributes() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Document getOwnerDocument() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node insertBefore(Node newChild, Node refChild) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node replaceChild(Node newChild, Node oldChild) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node removeChild(Node oldChild) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node appendChild(Node newChild) throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean hasChildNodes() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Node cloneNode(boolean deep) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void normalize() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isSupported(String feature, String version) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getNamespaceURI() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getPrefix() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setPrefix(String prefix) throws DOMException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getLocalName() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean hasAttributes() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getBaseURI() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public short compareDocumentPosition(Node other) throws DOMException {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getTextContent() throws DOMException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setTextContent(String textContent) throws DOMException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isSameNode(Node other) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String lookupPrefix(String namespaceURI) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isDefaultNamespace(String namespaceURI) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String lookupNamespaceURI(String prefix) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isEqualNode(Node arg) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getFeature(String feature, String version) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object setUserData(String key, Object data, UserDataHandler handler) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getUserData(String key) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
